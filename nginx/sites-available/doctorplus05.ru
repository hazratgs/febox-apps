server {

        listen 80;
	listen 443;
        server_name www.doctorplus05.ru doctorplus05.ru;
        root /var/www/doctorplus05.ru;
        try_files  $uri $uri/ @rewrite;
	index index.php;

        error_log /home/febox/logs/doctorplus05.ru.nginx.error.log;
	access_log /home/febox/logs/doctorplus05.ru.access.log;
        location ~* ^.+\.(jpg|jpeg|gif|png|ico|svg|js|css|txt|mp3|ogg|mpe?g|avi|zip|gz|bz2?|rar)$ {
                access_log off;
                expires 30d;
                break;
        }

        location @rewrite {
                rewrite  ^/(.*)$ /index.php?route=$1;
        }

        location ~ \.php$ {
                fastcgi_pass   web:9000;
                fastcgi_index  index.php;
                fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
                include        fastcgi_params;
        }
}

