server {
	listen 80;
	server_name baza-stroymarketrd.ru www.baza-stroymarketrd.ru;
	return 301 https://stroymarketrd.ru;
}

server {
	listen 443;
	server_name baza-stroymarketrd.ru www.baza-stroymarketrd.ru;
        return 301 https://stroymarketrd.ru;
}
