server {
	listen 80;
	server_name randevu05.ru www.randevu05.ru;
	return 301 https://$host$request_uri;
}

server {
        listen 443 ssl http2;
        server_name randevu05.ru  www.randevu05.ru;
        root /var/www/new.randevu05.ru;
        try_files  $uri $uri/ @rewrite;
	index index.php;	

        error_log /home/febox/logs/randevu05.ru.nginx.error.log;
	access_log /home/febox/logs/randevu05.ru.access.log;

	# SSL сертификаты
	ssl_certificate /etc/letsencrypt/live/randevu05.ru/fullchain.pem; 
        ssl_certificate_key /etc/letsencrypt/live/randevu05.ru/privkey.pem;	
	
	ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
        ssl_prefer_server_ciphers on;
	
	ssl_dhparam /etc/ssl/certs/dhparam.pem;
	ssl_ciphers 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA';
	ssl_session_timeout 1d;
        ssl_session_cache shared:SSL:50m;
	
	ssl_stapling on;
        ssl_stapling_verify on;
        add_header Strict-Transport-Security max-age=15768000;

        location ~ /.well-known {
                allow all;
        }

	location ~* ^.+\.(jpg|jpeg|gif|png|ico|svg|js|css|txt|mp3|ogg|mpe?g|avi|zip|gz|bz2?|rar)$ {
                access_log off;
                expires 30d;
                break;
        }
	
        location @rewrite {
                rewrite  ^/(.*)$ /index.php?route=$1;
        }

        location ~ \.php$ {
                fastcgi_pass   web:9000;
                fastcgi_index  index.php;
                fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
                include        fastcgi_params;
        }
}
