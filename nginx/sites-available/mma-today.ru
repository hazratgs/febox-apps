server {

        listen 80;
	listen 443;
        server_name mma-today.ru www.mma-today.ru;
        root /var/www/mma-today.ru;
        try_files  $uri $uri/ @rewrite;
	index index.php;

	error_log /dev/null crit;
	access_log off;

        location ~* ^.+\.(jpg|jpeg|gif|png|ico|svg|js|css|txt|mp3|ogg|mpe?g|avi|zip|gz|bz2?|rar)$ {
                access_log off;
                expires 30d;
                break;
        }

        location @rewrite {
                rewrite  ^/(.*)$ /index.php?route=$1;
        }

        location ~ \.php$ {
                fastcgi_pass   web:9000;
                fastcgi_index  index.php;
                fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
                include        fastcgi_params;
        }
}
